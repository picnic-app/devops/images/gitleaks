ARG GITLEAKS_VERSION=8.16.3

######
FROM ghcr.io/gitleaks/gitleaks:v${GITLEAKS_VERSION}

WORKDIR /usr/src
COPY gitleaks.toml ./

ENV GITLEAKS_CONFIG=/usr/src/gitleaks.toml
